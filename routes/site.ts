import express from 'npm:express'

const router = express.Router()

router.get('/site', (req, res) => {
  res.send('site index')
})

export default router
