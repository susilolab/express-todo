import { config } from 'https://deno.land/std@0.167.0/dotenv/mod.ts';

const conf = await config();
export default conf
