// deno run --allow-read --allow-net --allow-env main.ts
// import 'npm:@types/express@^4.17'
import express, { Express, Request, Response} from 'npm:express'
import { join } from 'https://deno.land/std@0.169.0/path/mod.ts'
import { config } from 'https://deno.land/std@0.167.0/dotenv/mod.ts';
import 'npm:mariadb';
import compression from 'npm:compression'
import morgan from 'npm:morgan'
import 'npm:ejs'
import site from './routes/site.ts'
import Todos from './models/Todos.ts'

const dirname = new URL('.', import.meta.url).pathname
const app: Express = express()
const conf = await config();
const port = conf['PORT']

app.set('view engine', 'ejs')
app.use(compression())
app.use(morgan('combined'))
app.use('/static', express.static(join(dirname, 'public')))
app.use(express.json())
app.get('/', (_req: Request, res: Response) => res.render('pages/index', {title: 'Express Todo'}));
app.post('/todos', async (req: Request, res: Response) => {
  const post = req.body
  const result = await Todos.create(post)
  res.json({msg: 'hello'})
})
app.patch('/todos/:id', async (req: Request, res: Response) => {
  const id = req.params.id
  const post = req.body
  const todo = await Todos.findByPk(id)
  if (todo === null) {
    res.status(404).json({error: 1, msg: 'Data tidak ditemukan!.', item: null})
  } else {
    res.json({error: 0, msg: 'Data ditemukan!.', item: todo})
  }
})
app.get('/todos', async (_req: Request, res: Response) => {
  const todos = await Todos.findAll({limit: 10})
  res.json(todos)
})
app.get('/todos/:id', async (req: Request, res: Response) => {
  const id = req.params.id
  const todo = await Todos.findByPk(id)
  if (todo === null) {
    res.status(404).json({error: 1, msg: 'Data tidak ditemukan!.', item: null})
  } else {
    res.json({error: 0, msg: 'Data ditemukan!.', item: todo})
  }
})
app.delete('/todos/:id', async (req: Request, res: Response) => {
  const id = req.params.id
  const todo = await Todos.findByPk(id)
  if (todo === null) {
    res.status(404).json({error: 1, msg: 'Data tidak ditemukan!.'})
    return
  }
  
  await todo.destroy()
  res.json({error: 0, msg: 'Data sukses!.'})
})
app.use(site)

app.listen(port, () => console.log(`express deno run on port ${port}`));
