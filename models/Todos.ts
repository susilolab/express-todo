import { DataTypes } from 'npm:sequelize'
import sequelize from './db.ts'
import dotenv from '../dotenv.ts'

const Todos = sequelize(dotenv).define('Todos', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  title: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  body: {
    type: DataTypes.STRING,
    allowNull: true,
  }
}, {
  modelName: 'Todos',
  timestamps: false,
  tableName: 'todos',
})

export default Todos
