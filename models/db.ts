import {DotenvConfig} from 'https://deno.land/std@0.167.0/dotenv/mod.ts'
import { Sequelize } from 'npm:sequelize'
import 'npm:mariadb';

// Koneksi database dan dialectnya
const sequelize = (conf: DotenvConfig): Sequelize => {
  return new Sequelize(
    conf['DB_NAME'],
    conf['DB_USER'],
    conf['DB_PASS'],
    {
      host: conf['DB_HOST'],
      dialect: 'mariadb',
      pool: {
        max: 5,
        acquire: 30000,
        idle: 10000,
      },
    },
  )
}
export default sequelize 
